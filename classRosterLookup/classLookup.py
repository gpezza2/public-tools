from ldap3 import Server, Connection, Reader, ALL, SUBTREE, Tls, ALL_ATTRIBUTES
import ssl
import gssapi
import getpass
import os

tls_config = Tls(validate=ssl.CERT_REQUIRED, version=ssl.PROTOCOL_TLSv1)
ldapURL = 'ldap://CDC01.ad.uillinois.edu'
#user = input('Username: ') + '@ad.uillinois.edu'
user = f"{os.getlogin()}@illinois.edu"
print(f"running as user: {user}")
passw = getpass.getpass("Password: ")
conn = Connection(ldapURL, user, passw, auto_bind=False)
conn.start_tls()
conn.bind()

crn = input('Enter class CRN: ')
dept = input('Enter class department: ')
#classNum = input('Enter class number: ')
searchbase= "OU=Class Rosters,OU=Register,OU=Urbana,DC=ad,DC=uillinois,DC=edu"
scope = SUBTREE
filter = f"(&(extensionAttribute4={crn})(objectClass=group)(extensionAttribute1={dept}))"

conn.search(search_base=searchbase,search_scope=scope,search_filter=filter,attributes = ALL_ATTRIBUTES)

results = conn.entries

for entry in results:
    print(entry['cn'])
#results

