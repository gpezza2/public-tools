# Description
This tool is intended to be used to lookup course roster groups names based on Department and CRN.
This is to be used in conjunction with the Class Sync option in the ICCP User management portal.

# How to Use
## Initial Setup
`module load python/3`

`git clone https://gitlab.engr.illinois.edu/gpezza2/public-tools.git`

`cd classRosterLookup`

`python3 -m venv classRoster-venv`

`source classRoster-venv/bin/activate`

`python3 -m pip install -r requirements.txt`

`deactivate`

Edit launcher.sh to have the name of your environment/paths

## Regular Use
`cd /path/to/where/git/repo/was/pulled/to/public-tools/classRosterLookup`

`./launcher.sh`


![Image of typical operation](images/classRosterLookup-image.jpg?raw=true)
